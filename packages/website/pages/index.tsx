import styled from 'styled-components';
import PageTitle from '../client/components/PageTitle';
import { HomeHero } from '../client/containers/HomeHero';
import Links from '../client/containers/Links';

const StyledPage = styled.div`
  .page {
  }
`;

export function Index() {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.styled-components file.
   */
  return (
    <StyledPage>
      <PageTitle title="Home" />
      <HomeHero />
      <Links />
    </StyledPage>
  );
}

export default Index;
