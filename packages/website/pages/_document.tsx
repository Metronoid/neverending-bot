/* eslint-disable react/display-name */
import { ReactElement } from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

export default class CustomDocument extends Document<{
  styleTags: ReactElement[];
}> {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();

    const page = renderPage(
      (App) => (props) => sheet.collectStyles(<App {...props} />)
    );

    const styleTags = sheet.getStyleElement();

    return { ...page, styleTags };
  }

  render() {
    const description = "Maak gebruik van onze programmeur expertise om meer uit uw bedrijf te halen";
    return (
      <Html lang="nl">
        <Head>{this.props.styleTags}</Head>
        <meta name="description" content={description}></meta>
        <meta property="og:title" content="Data analyse voor uw bedrijf"></meta>
        <meta property="og:type" content="article"></meta>
        {/* <meta property="og:image" content="/logo.png"></meta> */}
        <meta property="og:description" content={description}></meta>
        {/*<meta name="cmsmagazine" content=""*/}
        {/*<meta name="msapplication-TileColor" content=""*/}
        {/*<meta name="theme-color" content=""*/}
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
