import { AppProps } from 'next/app';
import { HelmetProvider } from 'react-helmet-async';
import React from 'react';
import Layout from '../client/containers/Layout';

import '../client/styles/main.scss';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <React.StrictMode>
      <HelmetProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </HelmetProvider>
    </React.StrictMode>
  );
}

export default CustomApp;
