/* eslint-disable sort-keys */

const zIndex = {
  strip: 100,
  layout: 101,
  topNav: 4,
};

export default zIndex;
