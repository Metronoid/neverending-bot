import { EMAIL } from './contact';

// External URLs
export const URLS = {
  contact: `mailto:${EMAIL}`,
  discord: 'https://discord.gg/4HvdaPbXen',
  linkedin: 'https://www.linkedin.com/company/verantobv',
  whatsapp: 'https://wa.me/+31623919175',
};
