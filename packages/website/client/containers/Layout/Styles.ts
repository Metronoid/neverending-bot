import styled from 'styled-components';

import colors from '../../styles/colors';
import zIndex from '../../styles/zIndex';
import { Container } from '../../components/Container';

const topNavHeight = '60px';
const footerHeight = '348px';

export const Layout = styled.div`
  position: relative;
`;

export const LayoutContent = styled.div`
  position:fixed;
  width:80%;
  left:10%;
  top:60px;
  min-height: calc(100vh - ${topNavHeight + footerHeight});
  background: ${colors.white};
`;

export const LayoutFooter = styled(Container)`
  @media (max-width: 768px) {
    margin-left: 0;
    margin-right: 0;
  }
`;

export const LayoutNavbarLogo = styled.div`
  position: fixed;
  top: 20px;
  left:72px;
  display: inline-block;
  pointer-events: auto;
  z-index: ${zIndex.layout};
`;

export const LayoutNavbarStaticLogo = styled(LayoutNavbarLogo)`
  position: relative;
  top: 0px;
  left:0px;
`;

export const LayoutNavbarStrip = styled.div`
  position: fixed;
  top: 20px;
  left:0px;
  right:0px;
  z-index: ${zIndex.strip};
`;

export const LayoutNavbarContainer = styled.div`
  padding: 0 25px;
`;

export const LayoutFooterWrapper = styled.div`
  background: ${colors.palette.neutral['075']};
  width:100%;
  position: fixed;
  top: 750px;
  display: flex;
  justify-content: center;
  padding: 48px 48px;

  @media (max-width: 768px) {
    justify-content: start;
  }
`;
