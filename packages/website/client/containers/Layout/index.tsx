import React from 'react';

import * as S from './Styles';
import Footer from '../Footer';
import { SFC } from '../../types/generic';
import { ScrollContent } from '../../components/ScrollContent';
import Logo from '../TopNav/TopNavLogo';
import Name from '../TopNav/TopNavName';
import NavbarToggleButton from '../TopNav/NavbarToggleButton';
import { GlobalScrollbar, MacScrollbar } from '@neverending/ui-components';

const Layout: SFC = ({ children }) => {
  return (
    <S.Layout data-scrollbar="true">
      {/* <ScrollContent heightL={950} heightS={1200}> */}
      <GlobalScrollbar />
      <MacScrollbar>
        <S.LayoutNavbarLogo>
          <Name/>
        </S.LayoutNavbarLogo>
        {/* <NavbarToggleButton/> */}
        <S.LayoutNavbarStrip>
          <S.LayoutNavbarContainer>
            <S.LayoutNavbarStaticLogo>
              <Logo/>
            </S.LayoutNavbarStaticLogo>
          </S.LayoutNavbarContainer>
        </S.LayoutNavbarStrip>
        <S.LayoutContent>{children}</S.LayoutContent>
        <S.LayoutFooterWrapper>
          <S.LayoutFooter>
            <Footer />
          </S.LayoutFooter>
        </S.LayoutFooterWrapper>
      {/* </ScrollContent> */}
      </MacScrollbar>
    </S.Layout>
  );
};

export default Layout;
