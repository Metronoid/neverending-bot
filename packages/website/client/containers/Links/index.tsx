import React from 'react';

import * as S from './Styles';
import breakpoints from '../../styles/breakpoints';
import { useWindowDimensions } from '../../hooks';
import EmojiIcon, { EmojiType } from '../../components/EmojiIcon';
import { URLS } from '../../constants/routes';

const Links = () => {
  const { width } = useWindowDimensions();

  const items = [
    {
      description: 'Stuur ons een bericht via WhatsApp',
      emoji: EmojiType.Whatsapp,
      emojiBgColor: '#25D366',
      enabled: true,
      isExternal: true,
      title: 'Whatsapp',
      url: URLS.whatsapp,
    },
    {
      description: 'Neem contact op via e-mail',
      emoji: EmojiType.Mail,
      emojiBgColor: '#0085FF',
      enabled: true,
      isExternal: true,
      title: 'Email',
      url: URLS.contact,
    },
    {
      description: 'Ontdek ons netwerk',
      emoji: EmojiType.LinkedIn,
      emojiBgColor: '#0a66c2',
      enabled: true,
      isExternal: true,
      title: 'LinkedIn',
      url: URLS.linkedin,
    },
    {
      description: 'Praat met onze community',
      emoji: EmojiType.Discord,
      emojiBgColor: '#4F52FF',
      enabled: true,
      isExternal: true,
      title: 'Discord',
      url: URLS.discord,
    },
  ];

  const getEmojiContainerSize = () => {
    if (width > parseInt(breakpoints.large, 10)) {
      return 176;
    }
    return 136;
  };

  const getEmojiSize = () => {
    if (width > parseInt(breakpoints.large, 10)) {
      return 64;
    }
    return 48;
  };

  const navigate = (isExternal: boolean, url: string) => {
    if (isExternal) {
      const target = url.includes('mailto:') ? '_self' : '_blank';
      window.open(url, target, 'noreferrer noopener');
    }
  };

  return (
    <S.Container>
      {items.map((item) => (
        <S.Item
          key={item.title}
          role="button"
          tabIndex={0}
          onClick={() => item.enabled && navigate(item.isExternal, item.url)}
        >
          <EmojiIcon
            emoji={item.emoji}
            size={getEmojiContainerSize()}
            emojiSize={getEmojiSize()}
            color={item.emojiBgColor}
            zoomOnHover
          />
          <S.ItemTitle>{item.title}</S.ItemTitle>
          <S.ItemDescription>{item.description}</S.ItemDescription>
        </S.Item>
      ))}
    </S.Container>
  );
};

export default Links;
