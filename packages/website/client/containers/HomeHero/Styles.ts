import styled from 'styled-components';

import { Container as SharedContainer } from '../../components/Container';
import colors from '../../styles/colors';
import { h3, b1 } from '../../styles/fonts';

export const Container = styled(SharedContainer)`
  align-items: center;
  display: flex;
  flex-direction: column;
  padding: 0px 0 80px;

  @media (max-width: 1366px) {
    padding: 0px 0px 80px;
  }

  @media (max-width: 992px) {
    padding: 0px 0px 48px;
  }

  @media (max-width: 768px) {
    padding: 0px 0px 24px;
  }
`;

export const Paragraph = styled.p`
  ${h3.regular}
  color: ${colors.palette.neutral['600']};
  line-height: 150%;
  margin-bottom: 0px;
  margin-top: 0px;
  max-width: 820px;
  text-align: center;

  @media (max-width: 992px) {
    ${b1.regular}
    max-width: 560px;
  }
`;
