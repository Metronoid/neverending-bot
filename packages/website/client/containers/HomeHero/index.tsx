import React, { FC } from 'react';
import { HomeHighlights } from './HomeHighlights';
import * as S from './Styles';

export const HomeHero: FC = () => {
  return (
    <S.Container>
      <HomeHighlights />
      <S.Paragraph>
        Veranto is een jong data analyse bedrijf met een focus op maatschapelijk
        relevante projecten. Onze specialisme zit in het verzamelen en verwerken
        van data geleverd door de klant of verkregen via onze technieken. Wij
        zijn op zoek naar relevante projecten om de samenleving te helpen en het
        gebruik van data toegankelijker te maken. Heeft u een connectie die ons
        kan helpen? Neem contact op!
      </S.Paragraph>
      {/* {tutorials.map(({ title, id }) => (
        <div key={id}>
          <Link href={`/${id}`}>{title}</Link>
        </div>
      ))} */}
    </S.Container>
  );
};
