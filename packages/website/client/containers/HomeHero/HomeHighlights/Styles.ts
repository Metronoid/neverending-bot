import colors from '../../../styles/colors';
import { d0, d2 } from '../../../styles/fonts';
import styled from 'styled-components';

export const Heading = styled.h1`
  ${d0.bold}
  color: ${colors.palette.neutral['800']};
  line-height: 130%;
  margin-bottom: 24px;
  text-align: center;
  transition: color 0.3s linear;

  @media (max-width: 992px) {
    ${d2.bold}
  }
`;
