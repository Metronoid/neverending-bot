import React, { FC } from 'react';
import { HightlightableText } from '../../../components/HightlightableText';
import * as S from './Styles';

export type HomeHighlightsProps = {
  delay?: number;
  color?: string;
};

export const HomeHighlights: FC<HomeHighlightsProps> = ({
  delay = 2000,
  color = '#00D26A',
}) => {
  const [highlightedHeading, setHighlightedHeading] = React.useState<number>(0);

  React.useEffect(() => {
    const headingInterval = setInterval(() => {
      setHighlightedHeading((curr) => (curr + 1) % 3);
    }, delay);

    return () => clearInterval(headingInterval);
  }, []);

  return (
    <S.Heading>
      <HightlightableText highlighted={highlightedHeading === 0} color={color}>
        Data Analytics
      </HightlightableText>
      ,{' '}
      <HightlightableText highlighted={highlightedHeading === 1} color={color}>
        Microservices
      </HightlightableText>{' '}
      en{' '}
      <HightlightableText highlighted={highlightedHeading === 2} color={color}>
        Artificial Intelligence
      </HightlightableText>
      .
    </S.Heading>
  );
};
