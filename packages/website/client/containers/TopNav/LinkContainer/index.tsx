import Link from 'next/link';
import React from 'react';

import * as S from './Styles';

export interface LinkContainerProps {
  href?: string;
  children;
}

const LinkContainer = ({ href = '/', children }: LinkContainerProps) => (
  <Link passHref={true} href={href}>
    <S.LinkContainer>
      {children}
    </S.LinkContainer>
  </Link>
);

export default LinkContainer;
