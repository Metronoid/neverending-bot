import React from 'react';
import * as S from './Styles';

const NavbarToggleButton = () => (
  <S.NavbarToggle>
    <S.ToggleButton>
      <span></span>
      <span></span>
    </S.ToggleButton>
  </S.NavbarToggle>
);

export default NavbarToggleButton;
