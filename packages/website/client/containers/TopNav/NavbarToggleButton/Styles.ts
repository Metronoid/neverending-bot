import zIndex from '../../../styles/zIndex';
import styled from 'styled-components';

export const NavbarToggle = styled.div`
  display: block;
  position:fixed;
  top:25px;
  right:25px;
  z-index: ${zIndex.layout};
  pointer-events: auto;
  transition: transform .4s,-webkit-transform .4s,-moz-transform .4s,-o-transform .4s;
`;

export const ToggleButton = styled.button`
  position:relative;
  display: inline-block;
  height: auto;
  margin: 0;
  padding: 0;
  color: inherit;
  background: 0 0;
  border: 0;
  border-radius: 0;
  outline: 0;
  top:2px;
  width:24px;
  overflow: visible;
  &:hover {
    text-decoration: none;
  }
  &:not(:disabled) {
    cursor: pointer;
  }
  &:before {
    content:"";
    position:absolute;
    top: -10px;
    left: -10px;
    right: -10px;
    bottom: -10px;
  }
  span {
    position: relative;
    display: block;
    width: 100%;
    height: 2px;
    margin 8px 0;
    top: 0;
    border-radius: 25px;
    background: currentColor;
    transition: top ease-out .15s .15s,transform ease-out .15s 0s,-webkit-transform ease-out .15s 0s,-moz-transform ease-out .15s 0s,-o-transform ease-out .15s 0s
  }
`
