import Image from 'next/image';
import React from 'react';
import VerantoLogo from '../../../../assets/svg/veranto-logo.svg';
import LinkContainer from '../LinkContainer';

export interface LogoProps {
  width?: number;
  height?: number;
}

const Logo = ({ width = 44, height = 32 }: LogoProps) => (
  <LinkContainer>
    <Image src={VerantoLogo} height={height} width={width} alt="" />
  </LinkContainer>
);

export default Logo;
