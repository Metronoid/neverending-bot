import Image from 'next/image';
import React from 'react';
import VerantoName from '../../../../assets/svg/veranto-name.svg';
import LinkContainer from '../LinkContainer';

export interface NameProps {
  width?: number;
  height?: number;
}

const Name = ({ width = 170, height = 41 }: NameProps) => (
  <LinkContainer>
    <Image src={VerantoName} height={height} width={width} alt="" />
  </LinkContainer>
);

export default Name;
