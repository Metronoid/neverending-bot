import colors from "../../../styles/colors";
import { h4 } from "../../../styles/fonts";
import styled from "styled-components";

export const Container = styled.ul`
  list-style-type: none;
  margin-right: 80px;
  padding-inline-start: 0;

  &:last-child {
    margin-right: unset;
  }
`;

export const Header = styled.li`
  ${h4.bold};
  color: ${colors.palette.neutral['800']};
  margin-bottom: 12px;
  text-transform: capitalize;
`;

export const Item = styled.li`
  ${h4.regular};
  color: ${colors.palette.neutral['800']};
  margin-bottom: 12px;
`;
