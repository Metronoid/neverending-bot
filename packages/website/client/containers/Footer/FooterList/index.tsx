import React, { FC } from 'react';

import * as S from './Styles';

interface ListItem {
  title: string;
}

export interface FooterListProps {
  header: string;
  items: ListItem[];
}

const FooterList: FC<FooterListProps> = ({ header, items }) => {
  return (
    <S.Container data-testid="FooterList">
      <S.Header>{header}</S.Header>
      {items.map((item) => (
        <S.Item key={item.title}>{item.title}</S.Item>
      ))}
    </S.Container>
  );
};

export default FooterList;
