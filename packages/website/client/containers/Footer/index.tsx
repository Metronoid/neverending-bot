import React, { FC, memo, useCallback } from 'react';
import FooterList from './FooterList';
import * as S from './Styles';

interface ComponentProps {
  className?: string;
}

const infoLists = [
  {
    header: 'Bedrijfslocatie',
    items: [
      {
        title: 'MR P.J Troelstraweg 147A',
      },
      {
        title: '8919 AA Leeuwarden',
      },
    ],
  },
  {
    header: 'Bedrijfsinformatie',
    items: [
      {
        title: 'KvK: 85191639',
      },
      {
        title: 'Btw: NL863540880B01',
      },
    ],
  },
  {
    header: 'Contactinformatie',
    items: [
      {
        title: 'Wander van der Wal',
      },
      {
        title: '+31 6 23 91 91 75',
      },
      {
        title: 'wander@veranto.nl',
      },
    ],
  },
];

const Footer: FC<ComponentProps> = ({ className }) => {
  const renderLists = useCallback(
    () =>
      infoLists.map((list) => (
        <FooterList header={list.header} key={list.header} items={list.items} />
      )),
    [],
  );

  return (
    <S.Container className={className} data-testid="Footer">
      {renderLists()}
    </S.Container>
  );
};

export default memo(Footer);
