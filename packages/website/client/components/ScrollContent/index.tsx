import React, { useEffect } from 'react';
import { useWindowDimensions } from '../../hooks';
import breakpoints from '../../styles/breakpoints';
import { SFC } from '../../types/generic';
import * as S from './Styles';

export interface ScrollContentProps {
  children: React.ReactNode;
  heightS: number;
  heightL: number;
}

export const ScrollContent: SFC<ScrollContentProps> = ({ children, heightS = 1400, heightL = 950 }) => {
  const [yScrollPos, setYScrollPos] = React.useState<number>(0);
  const [yThumbPos, setYThumbPos] = React.useState<number>(0);
  const [isScrolling, setScrolling] = React.useState<boolean>(false);
  const [thumbHeight, setThumbHeight] = React.useState<number>(32);
  const [isHovered, setIsHovered] = React.useState<boolean>(false);
  const [isThumbHovered, setIsThumbHovered] = React.useState<boolean>(false);
  const [isGrabbing, setIsGrabbing] = React.useState<number>(0);
  const [isRendered, setIsRendered] = React.useState<boolean>(true);

  const { width } = useWindowDimensions();

  useEffect(() => {
    let handle = null;
    const handleWheelScroll = (e: WheelEvent) => {
      if (handle) {
          clearTimeout(handle);
      }
      handle = setTimeout(() => {
        if(!isScrolling)
          setScrolling(false);
      }, 200);

      handleScroll(e.deltaY);
      setScrolling(e.deltaY != 0);
    }

    const handleGrab = (e: MouseEvent) => {
      if(isThumbHovered){
        setIsGrabbing(e.y);
        return;
      }
      if(isHovered)
        handleJump(-(e.y-(thumbHeight/2)));
    }

    const handleMove = (e: MouseEvent) => {
      if(isGrabbing){
        handleScroll(((e.y-isGrabbing)*4));
        setIsGrabbing(e.y)
      }
    }

    const handleRelease = (e: MouseEvent) => {
      setIsGrabbing(0);
    }

    const handleScroll = (deltaY: number) => {
      handleJump(yScrollPos - deltaY);
    }

    const handleJump = (y: number) => {
      let height = heightS;
      if(width > parseInt(breakpoints.medium, 10)){
        height = heightL;
      }
      const scrollHeight = height-window.innerHeight;
      const newYScrollPos = Math.min(Math.max(y, -scrollHeight), 0);
      const scrollDistance = -(newYScrollPos/(scrollHeight));
      setYScrollPos(newYScrollPos);
      setYThumbPos(scrollDistance*(window.innerHeight-thumbHeight));
    }

    const rerender = () => {
      setIsRendered.bind(false)
    }

    let height = heightS;
    if(width > parseInt(breakpoints.medium, 10)){
      height = heightL;
    }
    const newThumbHeight = window.innerHeight*(window.innerHeight/height);
    if(thumbHeight != newThumbHeight)
      setThumbHeight(newThumbHeight);
    window.onbeforeunload = function () {
      window.scrollTo(0, 0);
      setYScrollPos(0);
    }

    window.addEventListener('mousedown', handleGrab);
    window.addEventListener('mousemove', handleMove);
    window.addEventListener('mouseup', handleRelease);
    window.addEventListener('wheel', handleWheelScroll);
    window.addEventListener('resize', rerender);
    setIsRendered(true);
    return () => {
      window.removeEventListener('wheel', handleWheelScroll);
      window.removeEventListener('mousedown', handleGrab)
      window.removeEventListener('mousemove', handleMove)
      window.removeEventListener('mouseup', handleRelease)
      window.addEventListener('resize', rerender);
    }
    }, [thumbHeight, yScrollPos, isScrolling, isThumbHovered, isHovered, isGrabbing, heightS, width, heightL]);

  return(
  <>
    <S.ScrollContent style={{transform: `translate3d(0px, ${yScrollPos}px, 0px)`}}>{children}</S.ScrollContent>
    <S.ScrollbarTrackY
    onMouseEnter={() => setIsHovered(true)}
    onMouseLeave={() => setIsHovered(false)}
    show={isScrolling || isGrabbing != 0 || isHovered} style={{display: "block"}}>
    <S.ScrollbarThumb
    onMouseEnter={() => setIsThumbHovered(true)}
    onMouseLeave={() => setIsThumbHovered(false)}
    style={{height: `${thumbHeight}px`, transform: `translate3d(0px, ${yThumbPos}px, 0px)`}}/>
    </S.ScrollbarTrackY>
  </>
  )
};
