import styled, { css } from 'styled-components';

export const ScrollContent = styled.div`
  display: flow-root;
`;

export interface ScrollbarTrackProps {
  show?: boolean;
}

export const ScrollbarTrack = styled.div<ScrollbarTrackProps>`
  position: absolute;
  opacity: 0;
  z-index: 1;
  background: rgba(222, 222, 222, .75);
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  -webkit-transition: opacity 0.5s 0.5s ease-out;
          transition: opacity 0.5s 0.5s ease-out;
  ${(props) => {
    if(props.show)
    return css`
    opacity: 1;
    -webkit-transition-delay: 0s;
            transition-delay: 0s;
    `
  }}
  &:hover{
    opacity: 1;
    -webkit-transition-delay: 0s;
    transition-delay: 0s;
  }
`;

export const ScrollbarTrackX = styled(ScrollbarTrack)`
  bottom: 0;
  left: 0;
  width: 100%;
  height: 8px;
`

export const ScrollbarTrackY = styled(ScrollbarTrack)`
  top: 0;
  right: 3px;
  width: 8px;
  height: 100vh;
`

export const ScrollbarThumb = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 8px;
  height: 8px;
  background: rgba(0, 0, 0, .5);
  border-radius: 4px;
  &:hover{
    cursor:grab;
  }
`
