import styled from 'styled-components';

export type HightlightTextProps = {
  color?: string;
  highlighted?: boolean;
};

export const HightlightableText = styled.span<HightlightTextProps>`
  ${({ highlighted, color = '#00D26A' }) => highlighted && `color: ${color};`}
  transition: color 0.3s linear;
`;
