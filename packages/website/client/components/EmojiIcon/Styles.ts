import styled from 'styled-components';

type Props = {
  color: string;
  emojiSize: number;
  marginBottom: number;
  marginRight: number;
  size: number;
  transparentBackground: boolean;
  zoomOnHover: boolean;
};

// TODO: #39
export const WhiteFilter = styled.svg`
  filter: invert(99%) sepia(97%) saturate(21%) hue-rotate(211deg)
    brightness(104%) contrast(100%);
`;

export const Container = styled.div<Props>`
  align-items: center;
  background-color: ${(props) =>
    props.transparentBackground ? 'transparent' : props.color};
  border-radius: 50%;
  display: flex;
  font-size: ${(props) => props.emojiSize}px;
  height: ${(props) => props.size}px;
  justify-content: center;
  margin-bottom: ${(props) => props.marginBottom}px;
  margin-right: ${(props) => props.marginRight}px;
  width: ${(props) => props.size}px;

  ${(props) =>
    props.zoomOnHover &&
    `
    &:hover {
      ${InnerContainer} {
        transform: scale(1.1);
      }
    }
  `}
`;

export const InnerContainer = styled.div`
  transition: transform 0.2s;
`;
