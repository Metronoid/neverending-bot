import React from 'react';
import { Helmet } from 'react-helmet-async';
import { SFC } from '../../types/generic';

export interface PageTitleProps {
  ogDescription?: string;
  ogImageUrl?: string;
  ogTitle?: string;
  ogType?: 'website' | 'article' | 'profile' | 'book';
  ogUrl?: string;
  title: string;
}

export const defaultOg = {
  description: 'Software, Microservices and Artificial Intelligence.',
  type: 'website',
  url: 'https://www.veranto.nl',
};

const PageTitle: SFC<PageTitleProps> = ({
  ogDescription = defaultOg.description,
  ogTitle,
  ogType = defaultOg.type,
  ogUrl = defaultOg.url,
  title,
}) => {
  const titleToUse = `${title} | Veranto`;
  const ogTitleToUse = ogTitle || titleToUse;
  return (
    <Helmet>
      <title>{titleToUse}</title>
      <meta
        name="description"
        property="og:description"
        content={ogDescription}
      />
      <meta property="og:title" content={ogTitleToUse} />
      <meta property="og:type" content={ogType} />
      <meta property="og:url" content={ogUrl} />
    </Helmet>
  );
};

export default PageTitle;
