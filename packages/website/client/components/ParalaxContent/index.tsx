import React from 'react';
import { SFC } from '../../types/generic';

export const ParalaxContent: SFC = ({
  children,
}) => {
  return <>{children}</>;
};
