import * as path from 'path';
import { dependenciesMetadataKey, extendedObject } from '../../modules/default/decorators/extends';
import { ModuleEvent } from '../_types/moduleEvents';
import ModuleLoader from '../../moduleLoader';
import { findFiles } from "../../moduleLoader/findFiles";

export const CommandTypes = {
  DEFAULT: 'default',
  ARGUMENT: 'context'
};

class BaseState{
  protected module!: Module;
  protected stateSet = false;
  public event = ModuleEvent.unloaded;
  public repeatable = false;
  public executable = false;
  static Event: ModuleEvent;

  setModule(module: Module) {
    this.module = module;
    this.stateSet = true;
  }

  public init(callback: (() => void)): void{
    callback();
  }
  public execute(input: DefaultInput, callback: (output: ModuleOutput) => void): void{
    if (this.executable && this.stateSet) {
      this.module.notify(this.event, input, callback);
    } else {
      callback && callback({success: false});
    }
  }
}

class State extends BaseState {
  public previousState: BaseState | State = new BaseState();

  public findState(event: ModuleEvent): boolean {
    if(event === this.event){
      return true;
    }
    if(this.previousState instanceof State){
      return this.previousState.findState(event);
    }
    return false;
  }
}

class LoadModule extends State {
  static override Event = ModuleEvent.load;
  override event = LoadModule.Event;

  static Repeatable = false;
  override repeatable = LoadModule.Repeatable;
  override executable = false;

  public override init(callback: () => void): void {
    const loadFurtherDependencies = () => {
      const dependencies = Reflect.getOwnMetadata(dependenciesMetadataKey, this.module, this.module.name);
      if (dependencies && dependencies.length > 0) {
        const initNextDependency = () => {
          const module: ModuleInterface = dependencies.pop();
          if (module) {
            ModuleLoader.queuedModules.forEach( (item, index) => {
              if(item.name === this.module.name)  {ModuleLoader.queuedModules.splice(index,1);}
            });
            module.setState(ModuleEvent.load, initNextDependency);
          } else {
            this.setNextState(callback);
          }
        };
        initNextDependency();
      } else {
        this.setNextState(callback);
      }
    };
    this.module.notify(this.event, {}, loadFurtherDependencies);
  }

  setNextState = (callback: () => void) => {
    this.module.notify(this.event, {}, () => {
      let startingState = ModuleEvent.init;
      if(this.module.ifHooked(ModuleEvent.insert)){
        startingState = ModuleEvent.insert;
      }
      this.module.setState(startingState, callback);
    });
  };
}


class InsertingData extends State {
  static override Event: ModuleEvent = ModuleEvent.insert;
  override event = InsertingData.Event;

  static Repeatable = false;
  override repeatable = InsertingData.Repeatable;
  override executable = false;

  public override init(callback: () => void): void {
    let compendiums: string[] = [];
    compendiums = findFiles(`./modules/`, '').filter(file => file.endsWith(`${this.module.name}.json`));
    if (compendiums.length === 0)
    { this.module.setState(ModuleEvent.init, callback); }

    const initNextCompendium = () => {
      const compendium = compendiums.pop();
      if (compendium) {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const result = require(`${path.resolve(`modules`)}/${compendium}`);
        if (result.items) {
          const initNextItem = () => {
            const item = result.items.pop();
            if (item) {
              console.log(`Inserting ${item.name} from ${this.module.name}`);
              this.module.notify(this.event, item, initNextItem);
            } else {
              initNextCompendium();
            }
          };
          initNextItem();
        }
      } else {
        this.module.setState(ModuleEvent.init, callback);
      }
    };
    initNextCompendium();
  }
}

class InitializingModule extends State {
  static override Event: ModuleEvent = ModuleEvent.init;
  override event = InitializingModule.Event;

  static Repeatable = false;
  override repeatable = InitializingModule.Repeatable;
  override executable = false;

  public override init(callback: () => void): void {
    this.module.notify(this.event, {}, () => {
      //THIS ONE
      this.module.setState(ModuleEvent.execute, callback);
    });
  }
}

class ReadyToExecute extends State {
  static override Event: ModuleEvent = ModuleEvent.execute;
  override event = ReadyToExecute.Event;

  static Repeatable = false;
  override repeatable = InitializingModule.Repeatable;
  override executable = true;

  public override init(callback: () => void): void{
    console.log(`${this.module.name} loaded in and ready to be used`);
    callback();
  }
}

const states: { [moduleEvent in ModuleEvent]: typeof State } = {0: State, 1: LoadModule, 2:InsertingData, 3:InitializingModule, 4: ReadyToExecute};

//#region Constructor
class Module implements ModuleInterface{
  private state: State | BaseState = new BaseState;
  name: string;
  description = "";
  category = "";
  library = false;
  display = true;
  active = true;
  alias: string[] = [];
  guildOnly = true;
  paths: string[] = [];
  permission: string[] = [];
  accessible = true;
  children: ModuleInterface[] = [];
  private hooks:  { [moduleEvent in ModuleEvent]: {target: extendedObject, key: string}[] } = {0: [], 1: [], 2: [], 3:[], 4:[]};
  //hooks: Discord.Collection<ModuleEvents, {target: ModuleEvents, key: string}[] > = new Discord.Collection < ModuleEvents, {target: ModuleEvents, key: string}[] > ();

  constructor(Module: ModuleInterface) {
    this.name = Module.name;
    this.description = Module.description ?? this.description;
    this.display = Module.display ?? this.display;
    this.accessible = Module.accessible ?? this.accessible;
    this.active = Module.active ?? this.active;
    this.alias = Module.alias ?? this.alias;
    this.paths = Module.paths ?? this.paths;
    this.guildOnly = Module.guildOnly ?? this.guildOnly;
    this.permission = Module.permission ?? this.permission;
    this.library = Module.library ?? this.library;
    this.category = Module.category ?? this.category;
    if(this.library){
      this.display = false;
    }
  }
  path!: string;
  file!: string;

  setState(event: ModuleEvent, callback: (() => void)): void {
    const foundState = states[event];
    const state: State = new (foundState)();
    state.setModule(this);
    if (this.state instanceof State && this.state.findState(event) && !state.repeatable) {
      callback();
      return;
    }
    state.previousState = this.state;
    this.state = state;
    this.state.init(callback);
  }

  getState(): State | BaseState {
    return this.state;
  }

  use (command: DefaultInput, callback: (model: ModuleOutput) => void) {
    this.execute(command, (result) => {
      callback(result);
    });
  }

  execute: (input: DefaultInput, insertCallback: (output: ModuleOutput) => void) => void = (command: DefaultInput, insertCallback: (model: ModuleOutput) => void) => {
    if(this.state){
      this.state.execute(command, insertCallback);
    }else{
      // For whatever reason state is not initialized.
      this.setState(ModuleEvent.load, () => {
        if(this.state){
          this.state.execute(command, insertCallback);
        }else{
          // We give up.
          insertCallback && insertCallback({success: false});
        }
      });
    }
  };

  subscribe(event: ModuleEvent, target: extendedObject, key: string) {
    const hook = this.hooks[event];
    hook.push({ target, key });
  }

  unsubscribe(event: ModuleEvent, target: extendedObject, key: string) {
    this.hooks[event].filter(hook => hook.target !== target && hook.key !== key);
  }

  ifHooked(event: ModuleEvent) {
    const hooks = this.hooks[event];
    return hooks.length > 0;
  }

  notify = (event: ModuleEvent, input: DefaultInput, callback: (out: ModuleOutput) => void) => {
    const hooks = this.hooks[event];
    let index = 0;
    let lastResult: ModuleOutput = {success: false};
    const goThroughHook = (result: ModuleOutput) => {
      if (result.success || result.response) {
        lastResult = result;
      }
      if (result.success === false) {
        const hook = hooks[index];
        if (hook) {
          index++;
          const m = this.children.find(c => c.constructor.name === hook.target.constructor.name);
          if (m) {
            Object.getPrototypeOf(m)[hook.key](input, goThroughHook);
          } else {
            goThroughHook(result);
          }
        } else {
          callback(lastResult);
        }
      } else {
        callback(lastResult);
      }
    };
    goThroughHook({success: false});
  };
}

class DiscordModule extends Module implements DiscordModuleInterface{
  override use (command: DiscordInput, callback: (model: ModuleOutput) => void) {
    this.execute(command, (result) => {
      if (callback && result) {
        callback(result);
      }
    });
  }
}
//#endregion

declare global {
  class DefaultModuleInterface {
    setState(event: ModuleEvent, callback: () => void): void;
    getState(): State | BaseState;
    subscribe(event: ModuleEvent, target: extendedObject, key: string): void;
    ifHooked(event: ModuleEvent): boolean;
    guildOnly: boolean;
    constructor(Module: ModuleInterface)
    display: boolean;
    library: boolean;
    active: boolean;
    description: string;
    category: string;
    permission: string[];
    alias: string[];
    children: ModuleInterface[];
    path: string;
    file: string;
    name: string;
    execute: (command: DefaultInput, insertCallback: (output: ModuleOutput) => void) => void;
    paths: string[];
    accessible: boolean;
  }
  class ModuleInterface extends DefaultModuleInterface {
    use: (command: DefaultInput, insertCallback: (output: ModuleOutput) => void) => void;
  }

  class DiscordModuleInterface extends DefaultModuleInterface {
    use: (command: DiscordInput, insertCallback: (output: ModuleOutput) => void) => void;
  }
}

const _global = (global) as any;
_global.ModuleInterface = Module;
_global.DiscordModuleInterface = DiscordModule;
