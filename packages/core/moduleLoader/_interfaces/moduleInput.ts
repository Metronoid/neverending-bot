import * as Discord from 'discord.js';
import { Interaction } from '../../modules/discord/guild/slash/main';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface ModuleInput{
    type?: string;
  }
  // TODO: Have Input consists of multiple diffferent input types..
  interface DefaultInput extends ModuleInput {
    action?: string;
    module?: ModuleInterface;
    interaction?: Interaction;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args?: any[];
  }
  interface DiscordInput extends DefaultInput{
    message: Discord.Message;
  }
}
