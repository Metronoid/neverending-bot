import { Interaction } from '../../modules/discord/guild/slash/main';

declare global {
  interface ModuleOutput {
    result?: any;
    success: boolean;
    response?: string;
    interaction?: Interaction;
  }
}
