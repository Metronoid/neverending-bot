import * as path from 'path';

import './_classes/moduleGroup';
import { findFiles } from './findFiles';
import { ModuleEvent } from './_types/moduleEvents';

export default class ModuleLoader {
  static modules: ModuleInterface[] = [];
  static queuedModules: ModuleInterface[] = [];
  static files: string[] = [];

  static insertComponent(module: ModuleInterface, component: ModuleInterface) {
    module.children.push(component);
  }

  static loadModules() {
    this.catalogModules();

    for (const module of this.modules) {
      if (module.file) {
        module.paths = module.paths.map(p => `${p}/`);
        module.paths.push('');

        for (const p of module.paths) {
          const f = path.join(module.file, '../', p);
          const componentFiles = this.files.filter(file => file.endsWith('.js') && file.substring(0, file.lastIndexOf(path.sep) + 1) === f);
          for (const file of componentFiles) {
            this.loadComponent(file, module);
          }
        }
      }
    }
  }

  static catalogModules(){
    this.modules = [];
    this.files = findFiles(`modules`);

    const moduleFiles = this.files.filter(file => file.endsWith('module.json'));
    for (const file of moduleFiles) {
      const p = `${path.resolve()}/modules/${file}`;
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const module = new ModuleInterface(require(p));
      if (module.active) {
        module.file = file;
        module.path = p;
        this.modules.push(module);
      }
    }
  }

  static loadComponent (file: string, module: ModuleInterface) {
    const component = `${path.resolve()}/modules/${file}`;
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    let js = require(component);
    if (typeof js.default === 'function') {
      js = new js.default();
      js.name = js.constructor.name;
    }
    js.path = component;
    this.insertComponent(module, js);
  }

  static init () {
    this.queuedModules = Array.from(this.modules);
    const initNextModule = () => {
      const module = this.queuedModules.shift();
      if (module) {
        module.setState(ModuleEvent.load, initNextModule);
      }
    };
    initNextModule();
  }

  static resetReducer () {
    this.loadModules();
    this.init();
  }
}
