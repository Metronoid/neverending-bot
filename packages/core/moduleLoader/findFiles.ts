import * as fs from 'fs';
import * as path from 'path';


export function findFiles(dir: string, ignore = '_', files: string[] = []) {
  let foundFiles = fs.readdirSync(path.resolve(dir));
  files = files.concat(foundFiles);
  for (const file in foundFiles) {
    const fl = foundFiles[file];
    if (fs.lstatSync(path.join(path.resolve(dir), fl)).isDirectory()) {
      if (ignore !== '' && fl.includes(ignore)) { continue; }
      foundFiles = foundFiles.concat((findFiles(path.join(dir, fl), ignore, files)).map(x => path.join(fl, x)));
    }
  }
  return foundFiles;
}
