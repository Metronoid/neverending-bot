import moduleLoader from "./moduleLoader";
import { startSequalize } from "./sequalize";

moduleLoader.loadModules();
//We can only Initialize after we've enabled the database.
startSequalize(() => {
  moduleLoader.init();
});
