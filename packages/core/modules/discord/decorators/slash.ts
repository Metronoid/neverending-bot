import "reflect-metadata";
import ModuleLoader from "../../../moduleLoader";

export const slashMetadataKey = Symbol("slash");

export interface slashCommand{
  module: string;
  command: string;
  name: string;
  type: number;
  aliases?: string[];
  description: string;
  options?: slashCommandOption[];
}

export function SetSlashCommand(target: any, _: string, info: slashCommand) {
  const mod = ModuleLoader.modules.find(m => m.name?.toLowerCase() === target.parent.name?.toLowerCase());
  if (mod) {
    const slashCommands: slashCommand[] = Reflect.getOwnMetadata(slashMetadataKey, target.parent, target.parent.name) || [];

    const i = slashCommands.find(m => m.name === info.name);
    if (i) {
      i.constructor = info.constructor ?? i.constructor;
      i.command = info.command ?? i.command;
      i.aliases = info.aliases ?? i.aliases;
      i.description = info.description ?? i.description;
    } else {
      slashCommands.push(info as slashCommand);
    }
    Reflect.defineMetadata(slashMetadataKey, slashCommands, target.parent, target.parent.name);
  }
  return target;
}

function SlashCommand(name: string, description: string) {
  return (target: any, key: string) => {
    return SetSlashCommand(target, key, {constructor: target.constructor.name, command: key, name, description} as slashCommand);
  };
}

export interface slashCommandOption extends slashCommand {
  required: boolean;
}

export function SetSlashCommandOption(target: any, _: string, option: slashCommandOption) {
  const mod = ModuleLoader.modules.find(m => m.name?.toLowerCase() === target.parent.name?.toLowerCase());
  if (mod) {
    const slashCommands: slashCommand[] = Reflect.getOwnMetadata(slashMetadataKey, target.parent, target.parent.name) || [];
    const i = slashCommands.find(m => m.name === option.command);
    if (i) {
      if(!i.options){
        i.options = [];
      }
      i.options.push(option);
    }else{
      slashCommands.push({name: option.command, options: [option]} as slashCommand);
    }
    Reflect.defineMetadata(slashMetadataKey, slashCommands, target.parent, target.parent.name);
  }
  return target;
}

function SlashCommandOption(command: string, name: string, description: string, type: number, required = false) {
  return (target: any, key: string) => {
    return SetSlashCommandOption(target, key, {command: command, name, description, required, type} as slashCommandOption);
  };
}

declare global {
  function SlashCommand(description: string, usage?: string): any;
  function SlashCommandOption(command: string, name: string, description: string, type: number): any;
  function SlashCommandOption(command: string, name: string, description: string, type: number, required: boolean): any;
}

const _global = (global) as any;
_global.SlashCommand = SlashCommand;
_global.SlashCommandOption = SlashCommandOption;
