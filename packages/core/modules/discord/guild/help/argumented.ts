import * as Discord from 'discord.js';
import { ModuleEvent } from '../../../../moduleLoader/_types/moduleEvents';
import moduleLoader from '../../../../moduleLoader';
import { specialAliases } from '../../decorators/has';
import { EmbedWrapper } from '../../../discord/lib/embed/wrapper';
import getInfo from './_utility/getInfo';

export default class Info  {
  @ExtendModule('help')
  parent!: ModuleInterface;

  @UseModule('serverSettings')
  SETTINGS!: ModuleInterface;

  @UseModule('embed')
  EMBED!: ModuleInterface;

  @CommandInfo("Displays the available commands for @module.", "@module", true)
  @SlashCommandOption("help", "module", "The module you want to get help for.", 3)
  @HasAlias([specialAliases.arguments])
  @OnModuleEvent(ModuleEvent.execute)
  execute (input: DiscordInput, callback: (out: ModuleOutput) => void) {
    const args = input.args?.join('');
    if(args){
      const m = moduleLoader.modules.find(m => m.name.startsWith(args) && m.display && m.active);
      if(m){
        const module: ModuleInterface = m;
        if (this.SETTINGS?.use) {
          this.SETTINGS.use({ ...input, args: ['prefix'] }, (output) => {
            if (!this.EMBED) {
              callback({ success: false });
              return;
            }

            const PREFIX = output.result;

            input.action = 'wrapper';
            this.EMBED.use(input, (output) => {
              const embedWrapper: EmbedWrapper = output.result;
              const helpEmbed = new Discord.MessageEmbed()
                .setColor('#0099ff')
                .setTitle(`Command List for the ${module.name} module`)
                .setFooter('The @ symbol indicates a value that has to be given to the command')
                .setDescription(`These are all the command that are available to you by using ${PREFIX}${module.name}`);

              const itemSheet = (callback: any) => {
                const itemSheet = embedWrapper.createEmbedPage(helpEmbed);
                const infoField = itemSheet.createEmbedField('Commands');

                const infoArray = getInfo(PREFIX, module);
                infoArray.forEach(info => {
                  infoField.addRow(info.name, info.value);
                });

                itemSheet.addField(infoField);
                callback(itemSheet);
              };

              embedWrapper.addPage(itemSheet);
              if (input) {
                embedWrapper.build(input);
              }
            });

            callback({ success: true });
            return;
          });
          callback({ success: true });
          return;
        }
      }
    }
    callback({ success: false });
    return;
  }
}
