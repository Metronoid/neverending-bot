import { specialAliases } from "../../../../default/decorators/has";
import { commandInfo, infoMetadataKey } from "../../../../default/decorators/info";

const getInfo = (prefix: string, module: ModuleInterface, onlyImportant = false) => {
    const commandInfos: commandInfo[] = Reflect.getOwnMetadata(infoMetadataKey, module, module.name);
    let info: {name: string, value: string}[] = [];

    if (commandInfos) {
        commandInfos.forEach((cmd) => {
        if(!onlyImportant || cmd.important){
            let action = (cmd.aliases && cmd.aliases.length>0) ? cmd.aliases[0] : '';
            if(action === specialAliases.arguments || action === specialAliases.default)
                {action = '';}
            const name = `${prefix + module.name} ${action} ${(cmd.usage ?? '')} `;
            info.push({name, value: cmd.description ?? ''});
        }
        });
        info = info.sort((a,b) => (a.name.length - b.name.length));
    }

    return info;
};
export default getInfo;
