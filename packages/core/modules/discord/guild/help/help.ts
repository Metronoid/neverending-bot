import * as Discord from 'discord.js';
import moduleLoader from '../../../../moduleLoader';
import { ModuleEvent } from '../../../../moduleLoader/_types/moduleEvents';
import { specialAliases } from '../../../../modules/discord/decorators/has';
import { EmbedPage, EmbedWrapper } from '../../lib/embed/wrapper';
import getInfo from './_utility/getInfo';

export default class Help  {
  @ExtendModule('help')
  parent!: ModuleInterface;

  @UseModule('serverSettings')
  SETTINGS!: ModuleInterface;

  @UseModule('embed')
  EMBED!: ModuleInterface;

  @CommandInfo("Displays the available modules.")
  @HasAlias([specialAliases.default])
  @OnModuleEvent(ModuleEvent.execute)
  execute (input: DiscordInput, callback: (out: ModuleOutput) => void) {
    this.SETTINGS.use({ ...input, args: ['prefix']}, (output) => {
      if (!this.EMBED) {
        callback({ success: false });
        return;
      }

      const PREFIX = output.result;

      input.action = 'wrapper';
      this.EMBED.use(input, (output) => {
        const embedWrapper: EmbedWrapper = output.result;
        const helpEmbed = new Discord.MessageEmbed()
          .setColor('#0099ff')
          .setTitle('Modules List')
          .setTimestamp()
          .setFooter('The @ symbol indicates a value that has to be given to the command')
          .setDescription(`These are all the modules that are available to you by using the Prefix.
        Use ${PREFIX}${this.parent.name} with the name of the module to see all the available commands.`);

        const itemSheet = (callback: (out: EmbedPage) => void) => {
          const itemSheet = embedWrapper.createEmbedPage(helpEmbed);

          const categories: { [name: string]: ModuleInterface[] } = {};

          moduleLoader.modules.forEach((module: ModuleInterface) => {
            if (module.active && module.display && module.permission.length === 0) {
              if(!(module.category in categories)){
                categories[module.category] = [module];
              }else{
                categories[module.category].push(module);
              }
            }
          }
          );

          for (const category in categories) {
            const infoField = itemSheet.createEmbedField(`${category} Modules`);
            const modules = categories[category];
            modules.forEach(module => {
              const name = `${PREFIX ? PREFIX + module.name : module.name}`;
              infoField.addRow(name, module.description);

              const importantInfo = getInfo(PREFIX, module, true);
              importantInfo.forEach(info => {
                infoField.addRow(info.name, info.value);
              });
            });
            itemSheet.addField(infoField);
          }
          callback(itemSheet);
        };

        embedWrapper.addPage(itemSheet);
        embedWrapper.build(input);
      });
      callback({ success: true });
      return;
    });
    callback({ success: true });
    return;
  }
}
