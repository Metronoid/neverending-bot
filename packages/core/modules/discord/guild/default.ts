import moduleLoader from "../../../moduleLoader";
import * as Discord from 'discord.js';
import {
  specialAliases
} from "../../default/decorators/has";
import {
  Guild
} from "./_schemes/guild";
import { GuildMember, User } from "./_schemes/user";
import { ModuleEvent } from "../../../moduleLoader/_types/moduleEvents";

export default class CommandsController {
  //The CommandsController extends from the BASE module.
  @ExtendModule('guild')
  parent!: ModuleInterface;

  //This module makes use of the following modules.
  @UseModule('serverSettings')
  SETTINGS!: ModuleInterface;
  @UseModule('guildSettings')
  GUILD!: DiscordModuleInterface;
  @UseModule()
  ERROR!: ModuleInterface;
  @UseModule('slash')
  SLASH!: ModuleInterface;

  //This module stores the following objects.
  @Store()
  client?: Discord.Client;

  //#region Initialization
  @OnModuleEvent(ModuleEvent.load)
  initializeDiscordClient(_: DefaultInput, callback: (out: ModuleOutput) => void) {
    if(!this.client){
      this.getClientToken((output) => {
        this.initializeClient();
        if (output.result && this.client) {
        this.setClientEvents();
        this.client.login(output.result).then(() => {
          if(this.client){
            const guildsCache = this.client.guilds.cache.array();
            const nextGC = () => {
              const gc = guildsCache.pop();
              if(gc){
                Guild.findOrCreate({
                  where: {
                    id: gc.id
                  }
                }).then(() => {
                  nextGC();
                });
              }else{
                callback({success: true});
              }
            };
            nextGC();
          }else{
            callback({success: false});
          }
        });
      }
      });
    }else{
      callback({success: false});
    }
  }

  initializeClient() {
    this.client = new Discord.Client({
      disableMentions: "everyone"
    });
  }

  getClientToken(callback: (output: ModuleOutput) => any) {
    this.SETTINGS.use({
      action: 'get',
      args: ['token']

    }, (output) => {
      callback(output);
    });
  }

  setClientEvents() {
    const discordEvents = [{
      name: Discord.Constants.Events.CLIENT_READY,
      listener: async () => {
        this.client?.on(Discord.Constants.Events.MESSAGE_CREATE, (msg: Discord.Message) => {
          if (msg.author.bot) {
            return;
          }
          this.handleDiscordMessage({
            action: specialAliases.default,
            message: msg,
            args: []
          // eslint-disable-next-line @typescript-eslint/no-empty-function
          }, () => {});
        });
      }
    }];

    for (const event of discordEvents) {
      this.client?.on(event.name, event.listener as(...args: [] | [Discord.Message] | [Discord.Channel]) => void);
    }
  }
  //#endregion

  //#region Command Execution
  @OnModuleEvent(ModuleEvent.execute)
  handleDiscordMessage(command: DiscordInput, callback: (output: ModuleOutput) => any) {
    if (command.message) {
      if (command.message.channel.type === 'dm') {
        command.message.reply("The bot can currently not be used in DMs.");
        return;
      }

      if (command.message.guild){
        const guild = command.message.guild;
        const userId = command.message.author.id;
        Guild.findOne({
          where: {
            id: guild.id,
          }
        }).then((guild) => {
          if(guild){
            User.findOrCreate({
              where: {
                id: userId
              }
            }).then(([user]) => {
              GuildMember.findOrCreate({
                where:{
                  UserId: user.id,
                  GuildId: guild.id
                }
              });
            });
          }
        });
      }

      this.getPrefix(command.message, (output) => {
        //Get prefix used to signal that a message is a command.
        if (output.result) {
          let finalResponse!: ModuleOutput;
          const prefix = output.result;
          if (command.message && command.args && (command.message.content.startsWith(prefix) && !command.message.author.bot)) {
            const args: string[] = [...command.args, ...this.getArgumentsFromMessage(command.message, prefix)];

            //Define commandName
            let commandName = args.shift();

            if (args.length > 0) {
              command.action = specialAliases.arguments;
            }

            if (commandName) {
              //command.message.channel.startTyping();
              commandName = commandName.toLowerCase();

              //Get the modules the correspond to the commandName
              const modules = this.getModules(commandName);

              const error = () => {
                this.ERROR && this.ERROR.use && this.ERROR.use({
                  ...command,
                  args: [
                    '101'
                  ]
                }, (code) => {
                  if (code.success && command.message && 'reply' in command.message) {
                    command.message.reply(code.result.description);
                  }
                });
              };

              if (modules.length === 0) {
                error();
                return {
                  success: false
                };
              }

              //Try every modules that corresponds until the right one is found.
              const func = (modules: ModuleInterface[]) => {
                if (modules.length > 0) {
                  const module = modules.pop();
                  if (!module) {
                    error();
                  }

                  command = {
                    ...command,
                    module,
                    args
                  };
                  //Check if module can be used in DMs.
                  if (command.module && command.module.guildOnly && command.message.channel.type === 'dm') {
                    //If not send an error message.
                    error();
                  }

                  try {
                    module?.use(command, (result: ModuleOutput) => {
                      if (result && result.response) {
                        finalResponse = result;
                      }
                      callback(result);
                      func(modules);
                    });
                  } catch (error: any) {
                    command.message.channel.send(`${module?.name} crashed. \n${error.message}`);
                  }
                } else {
                  if (finalResponse && finalResponse.response) {
                    if(finalResponse.interaction){
                      // eslint-disable-next-line @typescript-eslint/no-empty-function
                      this.SLASH.use({...finalResponse, action: 'interaction', args: [finalResponse.response]}, () => {});
                    }else{
                      command.message.channel.send(finalResponse.response);
                    }
                  }
                }
              };
              func(modules);
              //command.message.channel.stopTyping();
            }
          }
        }
        return;
      });
    }
    return;
  }

  getPrefix(message: Discord.Message ,callback: (output: ModuleOutput) => any) {
    this.GUILD.use({
      message,
      action: 'get',
      args: [
        'prefix'
      ]
    }, (output) => {
      callback(output);
    });
  }

  getArgumentsFromMessage(message: Discord.Message, prefix = '') {
    return message.content.slice(prefix.length).split(/ +/);
  }

  getModules(commandName: string) {
    return moduleLoader.modules.filter((mod: ModuleInterface) => mod.accessible && (mod.name.startsWith(commandName) || mod.alias && mod.alias.includes(commandName))).map(m => m);
  }
  //#endregion
}
