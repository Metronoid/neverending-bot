import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class MessageCleaner  {
  @ExtendModule('mod')
  parent!: ModuleInterface;

  @Permission(['Rat Catcher', 'bot'])
  @HasAlias(['clean', 'clear'])
  @OnModuleEvent(ModuleEvent.execute)
  execute(input: DiscordInput, callback: (out: ModuleOutput) => void) {
    if (input.args && input.args.length > 0) {
      input.message.channel.messages.fetch()
        .then((messages) => {
          messages = messages.filter(m => input.args !== undefined && input.args[1] !== undefined && m.author.id === input.args[1]);
          messages.forEach(element => {
            element.delete().catch(error => {
              if (error.code !== 10008) {
                console.error('Failed to delete the message:', error);
              }
            });
          });
        })
        .catch(console.error);
      callback({ success: true });
      return;
    }
    callback({ success: false });
    return;
  }
}
