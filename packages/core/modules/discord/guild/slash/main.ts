import * as Discord from 'discord.js';
import { Message, TextChannel, User } from "discord.js";
import { ModuleEvent } from '../../../../moduleLoader/_types/moduleEvents';
import moduleLoader from '../../../../moduleLoader';
import { slashCommand, slashMetadataKey } from "../../decorators/slash";

export interface Interaction {
  data: {
      name: string;
      options: [{
        value: string
      }]
  };
  guild_id: any;
  channel_id: any;
  member: { user: User; };
  id: any;
  token: any;
}

export default class SlashController  {
  @ExtendModule('slash')
  parent!: ModuleInterface;

  @Store('guild')
  client!: Discord.Client;

  @UseModule('guild')
  GUILD!: DiscordModuleInterface;

  @UseModule('serverSettings')
  SETTINGS!: ModuleInterface;

  @UseModule()
  REQUEST!: ModuleInterface;

  @OnModuleEvent(ModuleEvent.init)
  updateSlashCommands(_: DefaultInput, callback: (out: ModuleOutput) => void) {
    if(this.parent.children){
      const SlashGetter = this.parent.children.find(c => c.name === 'SlashGetter');
      const SlashSetter = this.parent.children.find(c => c.name === 'SlashSetter');
      if(SlashGetter){
        moduleLoader.modules.forEach((module: any) => {
          if (module.display && module.active && !module.permission) {
            SlashGetter?.execute({
              action: 'get', args: []
            }, (output: ModuleOutput) => {
              if(output && output.result){
                // Loop through slashCommands and add those that are new or have been changed.
                const slashCommands: slashCommand[] = Reflect.getOwnMetadata(slashMetadataKey, module, module.name);
                slashCommands?.forEach((slashCommand: slashCommand) => {
                  const similarCommand = output.result.find((s: any) => s['name'] === slashCommand.name && s['description'] === slashCommand.description);
                  if(!similarCommand){
                    SlashSetter?.execute({
                      action: 'add',
                      args: [slashCommand.name, slashCommand.description, JSON.stringify(slashCommand.options)]
                    // eslint-disable-next-line @typescript-eslint/no-empty-function
                    }, () => {});
                  }
                });
              }
            });
          }
        });
      }
    }
    callback({success: true});
  }

  @OnModuleEvent(ModuleEvent.init)
  setupInteractionHandler(_: any, callback: (out: ModuleOutput) => void) {
    this.client && this.client.ws.on('INTERACTION_CREATE' as any, async (interaction: Interaction) => {
      if (interaction.data && interaction.data.name) {
        this.SETTINGS.use({ action: 'get', args: ['prefix'] }, (output) => {
          const PREFIX = output.result;
          const command = `${PREFIX}${interaction.data.name?.toLowerCase()}`;
          this.client.guilds.fetch(interaction.guild_id).then((guild: any) => {
            const channel = guild.channels.cache.get(interaction.channel_id) as TextChannel;
            const fakeMessage = { content: command, author: { ...interaction.member.user, bot: false }, channel } as Message;
            const options = interaction.data.options?.map(o => o.value);
            this.GUILD.use({
              action: 'default',
              message: fakeMessage,
              args: options ?? [],
              interaction
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            }, () => {});
          });
        });
      }
    });
    callback({success: true});
  }

  @HasAlias(['interaction'])
  @OnModuleEvent(ModuleEvent.execute)
  InteractionResponse(command: DefaultInput, callback: (out: ModuleOutput) => any){
    if(command.interaction){
      (this.client as any).api.interactions(command.interaction.id, command.interaction.token).callback.post({
        data: {
          type: 4,
          data: {
            content: command.args && command.args.length > 0 ? command.args[0] : '\u200b'
          }
        },
      }).then(() => {
        callback({success: true});
      });
    }else{
      callback({success: false});
    }
  }
}
