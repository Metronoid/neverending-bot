import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class SlashRemover  {
  @ExtendModule('slash')
  parent!: ModuleInterface;

  @UseModule()
  REQUEST!: ModuleInterface;

  @HasAlias(['remove', 'r', 'd'])
  @OnModuleEvent(ModuleEvent.execute)
  execute (command: DiscordInput, callback: (arg: ModuleOutput) => any) {
    if (command.args && command.args.length > 1) {
      const headers = {
        "Authorization": "Bot NzQ5OTM2NDc1MDUwODY4ODI2.X0zOxA.VHvUnqgithkvMFnPHcpa7-CdtVU"
      };

      this.REQUEST.use({ ...command, type: 'DELETE', args: [`https://discord.com/api/v8/applications/749936475050868826/commands/${command.args[1]}`, JSON.stringify(headers)] }, (output) => {
        if (output.success && command.args) {
          command.message.reply(`Removed ${command.args[1]}`);
        }
      });
    }

    callback({ success: true });
  }
}
