import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class SlashGetter {
  @ExtendModule('slash')
  parent!: ModuleInterface;

  @UseModule()
  REQUEST!: ModuleInterface;

  @HasAlias(['get'])
  @OnModuleEvent(ModuleEvent.execute)
  execute(command: DefaultInput, callback: (arg: ModuleOutput) => any){
    if (!this.REQUEST) {
      callback && callback({ success: false });
      return;
    }

    const headers = {
      "Authorization": "Bot NzQ5OTM2NDc1MDUwODY4ODI2.X0zOxA.VHvUnqgithkvMFnPHcpa7-CdtVU"
    };

    this.REQUEST.use({ ...command, action: 'get', args: [`https://discord.com/api/v8/applications/749936475050868826/commands`, JSON.stringify(headers)] }, (output) => {
      if (output.success) {
        let response = "";
        for (const res of output.result) {
          response += `\n${res['name']}: ${res['description']} (${res['id']})`;
        }

        callback && callback({ success: true, result: output.result, response });
      }else{
        callback && callback({ success: false });
      }
    });
  }
}
