import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class SlashSetter  {
  @ExtendModule('slash')
  parent!: ModuleInterface;

  @UseModule()
  REQUEST!: ModuleInterface;

  @HasAlias(['post', 'add', 'set'])
  @OnModuleEvent(ModuleEvent.execute)
  execute(command: DefaultInput, callback: (arg: ModuleOutput) => any) {
    if (command.args && command.args.length > 2) {
      const headers = {
        "Authorization": "Bot NzQ5OTM2NDc1MDUwODY4ODI2.X0zOxA.VHvUnqgithkvMFnPHcpa7-CdtVU"
      };

      const json = {
        "name": command.args[0],
        "description": command.args[1],
        "options": command.args[2],
      };

      this.REQUEST.use({ ...command, type: 'post', action: 'post', args: [`https://discord.com/api/v8/applications/749936475050868826/commands`, JSON.stringify(headers), JSON.stringify(json)] }, (output: { success: any; }) => {
        if (output.success && command.args) {
          console.log(`Added ${command.args[0]} as a SlashCommand`);
        }
      });
    }
    callback({ success: true });
  }
}
