import * as Discord from 'discord.js';
import { GuildSettings, GuildSettingsInterface } from "./_schemes/guildSettings";
import { prefix } from "../../../../localSettings.json";
import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class GuildSettingsModule {
  @ExtendModule('guildSettings')
  parent!: any;

  @Store()
  settings?: GuildSettingsInterface[];

  @Store('guild')
  client?: Discord.Client;

  @OnModuleEvent(ModuleEvent.init)
  init(_: DefaultInput, callback: (out: ModuleOutput) => void) {

    if(this.client){
      const guilds = this.client.guilds.cache.array();
      const createNext = () => {
         const create = (guild: Discord.Guild, name: string, value: string) => {
          GuildSettings.findOrCreate({
            where: {
              GuildId: guild.id,
              name
            },
            defaults: {
              value,
              active: true
            }
          }).then(() => {
            createNext();
          });
        };

        const g = guilds.pop();
        if(g){
          create(g, "prefix", prefix);
        }else{
          callback({success: true});
        }
      };
      createNext();
    }else{
      callback({success: false});
    }
  }

  @OnModuleEvent(ModuleEvent.execute)
  execute(command: DiscordInput, insertCallback: (out: ModuleOutput) => void){
    const returnCallback = (command: DefaultInput, insertCallback: (out: ModuleOutput) => void) => {
      // Try to find the setting in our store.
      let setting: GuildSettingsInterface | undefined = undefined;
      if(this.settings)
        {setting = this.settings.find((setting: GuildSettingsInterface) => command.args ? setting.name === command.args[0]: false);}
      insertCallback({ success: setting !== undefined, result: setting?.value });
    };

    if (command.message.guild && (!this.settings || this.settings.length === 0)) {
      GuildSettings.findAll({
        where: {
          GuildId: command.message.guild.id,
          active: true
        }
      }).then((result: any[]) => {
        if (result) {
          this.settings = result;
          returnCallback(command, insertCallback);
        }
      });
      return;
    }else{
      returnCallback(command, insertCallback);
    }
  }
}
