import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../../../../sequalize/index.js';
import { Guild } from '../../_schemes/guild.js';

export interface GuildSettingsInterface extends Model {
  id: number;
  name: string;
  GuildId: number;
  active: boolean;
  value: string;
}

export const GuildSettings = sequelize.define<GuildSettingsInterface>('GuildSettings', {
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: DataTypes.STRING(32),
    allowNull: false,
    unique: false,
  },
  active: {
    type: DataTypes.BOOLEAN,
    allowNull: false
  },
  value: {
    type: DataTypes.STRING(2024),
    allowNull: false
  }
});
GuildSettings.belongsTo(Guild);
