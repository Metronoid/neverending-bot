import { sequelize } from '../../../../sequalize';
import { DataTypes, Model } from 'sequelize';

interface ChannelInterface extends Model {
  name: string;
  text: string;
  DescriptionText: string;
  Description: string;
}

export const Channel = sequelize.define<ChannelInterface>('Channel', {
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING(32),
  },
}, {
});
