import { sequelize } from '../../../../sequalize';
import { DataTypes, Model } from 'sequelize';
import { Guild } from './guild';

export interface UserInterface extends Model {
  id: string;
  username: string;
  tag: string;
  avatar: string;
}

export const User = sequelize.define<UserInterface>('User', {
  id: {
    type: DataTypes.STRING(64),
    primaryKey: true
  },
  tag: {
    type: DataTypes.STRING(64)
  },
  username: {
    type: DataTypes.STRING(64)
  },
  avatar: {
    type: DataTypes.STRING(1024)
  }
}, {});

export interface GuildMemberInterface extends Model {
  nickname: string;
}

export const GuildMember = sequelize.define<GuildMemberInterface>('GuildMember', {
  nickname: {
    type: DataTypes.STRING(64)
  },
}, {});
User.belongsToMany(Guild, {through: GuildMember});
