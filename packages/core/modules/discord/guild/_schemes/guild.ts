import { sequelize } from '../../../../sequalize';
import { DataTypes, Model } from 'sequelize';
export interface GuildInterface extends Model {
  id: string;
}

export const Guild = sequelize.define<GuildInterface>('Guild', {
  id: {
    type: DataTypes.STRING,
    primaryKey: true
  }
}, {});
