import { sequelize } from '../../../../sequalize';
import { DataTypes, Model } from "sequelize";
import { User, UserInterface } from './user';

export interface UserStatsInterface extends Model {
    UserId: string;
    User: UserInterface;
    voiceMinutes: number;
}

export const UserStat = sequelize.define<UserStatsInterface>('UserStat', {
    UserId: {
        type: DataTypes.STRING(64),
        primaryKey: true
    },
    voiceMinutes: {
        type: DataTypes.INTEGER
    }
}, {});

UserStat.belongsTo(User);
