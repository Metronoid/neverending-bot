import { User } from '../../guild/_schemes/user';
import { Op } from 'sequelize';
import * as Discord from 'discord.js';
import { ModuleEvent } from '../../../../moduleLoader/_types/moduleEvents';

export default class UserDatabase {
  @ExtendModule('database')
  parent!: ModuleInterface;

  @Store('guild')
  client!: Discord.Client;

  @OnModuleEvent(ModuleEvent.execute)
  @HasAlias(['user'])
  execute (command: DiscordInput, callback: (out: ModuleOutput) => void) {
    let args = command.args?.join('').replace(/<@(.*)>/, "$1");
    if (args === '' && command.message) {
      args = command.message.author.id;
    }
    if (args) {
      if (!isNaN(parseInt(args, 10))) {
        User.findOne({
          where: {
            id: args,
          }
        }).then(user => {
          if (command.message && user && user.id === command.message.author.id) {
            command.message.author.tag ? user.tag = command.message.author.tag : '';
            command.message.author.username ? user.username = command.message.author.username : '';
            command.message.author.displayAvatarURL ? user.avatar = command.message.author.displayAvatarURL() : '';
            user.save();
          } else {
            if (this.client && user && args) {
              this.client.users.fetch(args).then((u) => {
                user.avatar = u.avatar ? u.avatar : user.avatar;
                user.username = u.username ? u.username : user.username;
                user.tag = u.tag ? u.tag : user.tag;
                user.save();
              });
            }
          }
          callback({ success: user !== null, result: user });
        });
      } else {
        User.findOne({
          where: {
            [Op.or]: {
              username: { [Op.iLike]: `%${args}%` }
            }
          }
        }).then(user => {
          callback({ success: user !== null, result: user });
        });
      }
    }
  }
}
