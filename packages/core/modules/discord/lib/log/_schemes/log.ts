import { sequelize } from "./../../../../../sequalize";
import { DataTypes, Model } from "sequelize";

export interface LogInterface extends Model{
  event: string
  type: string
  user: number
  value: string
  channel: number
}

export const Log = sequelize.define<LogInterface>('Log', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
},
  event: {
    type: DataTypes.STRING,
    allowNull: false
  },
  type: {
    type: DataTypes.STRING,
  },
  userId: {
    type: DataTypes.BIGINT,
  },
  channelId: {
    type: DataTypes.BIGINT,
  },
  value: {
    type: DataTypes.STRING(2000),
  }
}, {});
