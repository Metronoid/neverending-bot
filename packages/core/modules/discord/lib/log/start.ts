import { Log } from "./_schemes/log";
import * as Discord from 'discord.js';
import { LOG_EVENTS } from "./_types/discordLogTypes";
import { log } from "./_types/log";
import { User } from "../../guild/_schemes/user";
import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

class LogHandler {
  @ExtendModule('log')
  parent!: ModuleInterface;

  @UseModule('BASE')
  base!: ModuleInterface;

  @Store('guild')
  client!:  Discord.Client;

  @OnModuleEvent(ModuleEvent.init)
  init(_: DefaultInput, callback: (out: ModuleOutput) => void) {
    if (!this.client) {
      callback && callback({success: false});
      return;
    }

    function createLog(log: log) {
      Log.create({...log});
    }

    const discordEvents: any = [
      // {
      //   name: Discord.Constants.Events.CHANNEL_CREATE,
      //   listener: async (channel: Discord.Channel) => {
      //     createLog({ event: LOG_EVENTS.CHANNEL_CREATE, type: channel.type, channel: channel.id });
      //   }
      // },
      // {
      //   name: Discord.Constants.Events.CHANNEL_DELETE,
      //   listener: async (channel: Discord.Channel) => {
      //     createLog({ event: LOG_EVENTS.CHANNEL_CREATE, channel: channel.id });
      //   }
      // },
      // {
      //   name: Discord.Constants.Events.CHANNEL_PINS_UPDATE,
      //   listener: async (channel: Discord.Channel) => {
      //     createLog({ event: LOG_EVENTS.CHANNEL_PINS, channel: channel.id });
      //   }
      // },
      // {
      //   name: Discord.Constants.Events.CHANNEL_UPDATE,
      //   listener: async (oldChannel: Discord.Channel, newChannel: Discord.Channel) => {
      //     createLog({ event: LOG_EVENTS.CHANNEL_UPDATE_BEFORE, channel: oldChannel.id });
      //     createLog({ event: LOG_EVENTS.CHANNEL_UPDATE_AFTER, channel: newChannel.id });
      //   }
      // },
      {
        name: Discord.Constants.Events.CLIENT_READY,
        listener: async () => {
          createLog({ event: LOG_EVENTS.READY });
        }
      },
      {
        name: Discord.Constants.Events.VOICE_STATE_UPDATE,
        listener: async (oldState: Discord.VoiceState, newState: Discord.VoiceState) => {
          const newUserChannel = newState.channel;
          const oldUserChannel = oldState.channel;

          if(oldState.member){
            User.findOrCreate({
              where: {
                id: oldState.member.id
              }
            }).then(([user]) => {
              if(oldState.member){
                user.tag = oldState.member.user.tag;
                user.username = oldState.member.user.username;
                user.avatar = oldState.member.user.displayAvatarURL();
                user.save();

                createLog({ event: LOG_EVENTS.VOICE_STATE_UPDATE, channel: newUserChannel?.id, type: !oldUserChannel ? 'joins' : 'leaves', user: oldState.member.user.id });
              }
            });
          }
        }
      },
      // {
      //   name: Discord.Constants.Events.MESSAGE_CREATE,
      //   listener: async (msg: Discord.Message) => {
      //     createLog({ event: LOG_EVENTS.MESSAGE, user: msg.author.id, channel: msg.channel.id, value: msg.content });
      //   }
      // }
    ];

    for (const event of discordEvents) {
      this.client.on(event.name, event.listener);
    }

    callback && callback({success: true});
  }
}

module.exports = new LogHandler();
