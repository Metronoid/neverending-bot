export interface log {
  event: string
  type?: string
  channel?: string
  user?: string
  value?: string
}
