import * as Discord from 'discord.js';
import { ModuleEvent } from '../../../../moduleLoader/_types/moduleEvents';

export default class WrapperFactory {
  @ExtendModule('embed')
  parent!: ModuleInterface;

  @UseModule('slash')
  slash!: ModuleInterface;

  @OnModuleEvent(ModuleEvent.execute)
  execute(command: DiscordInput, callback: (out: ModuleOutput) => void){
    if (command.message) {
      callback({ success: true, result: new EmbedWrapper(command.message.author.id, this.slash) });
      return;
    }
    callback({ success: false });
  }
}

export class EmbedWrapper {
  pages: ((callback: (page: any) => any) => void)[] = [];
  createdPages: EmbedPage[] = [];
  private prevPageNumber = 1;
  pageNumber = 1;
  user: string;
  slash: ModuleInterface;
  message?: Discord.Message;
  public currentInteractions: EmbedReaction[] = [];

  constructor(user: string, slash: ModuleInterface) {
    this.user = user;
    this.slash = slash;
  }

  addPage(page: (callback: (page: any) => any) => void) {
    this.pages.push(page);
    this.update();
  }

  replace(wrapper: EmbedWrapper) {
    this.pages = wrapper.pages;
    this.prevPageNumber = 0;
    this.pageNumber = 1;
    this.update();
  }

  replacePage(original: number, page: (callback: (page: any) => any) => void) {
    this.pages[original - 1] = page;
    if (original === this.prevPageNumber) {
      this.prevPageNumber = 0;
    }
    this.update();
  }

  build(command: DiscordInput): void;
  build(message: Discord.Message, dm?: boolean): void;
  build(source: Discord.Message | DiscordInput, dm?: boolean) {
    if("message" in source && source.message) {
      if(source.interaction){
        // Acknowledge the message so we can edit it later.
        this.slash.use({...source, action: 'interaction'}, () => {
          if(source.message){
            const lastMessage = source.message.channel.lastMessage;
            if(lastMessage){
              this.message = lastMessage;
            }
            this._build(source.message, dm);
          }
        });
      }else{
        this._build(source.message, dm);
      }
    }else{
      this._build(source as Discord.Message, dm);
    }
  }

  _build(message: Discord.Message, dm?: boolean) {
    if (this.pages.length >= this.pageNumber) {
      this.pages[this.pageNumber - 1]((page: EmbedPage) => {
        if (page) {
          if (message && !this.message) {
            const promise = dm ? message.author.send(page.build()) : message.channel.send(page.build());
            promise.then((message) => {
              this.message = message;
              page.postBuild();
            });
          } else {
            this.message && this.message.edit(page.build()).catch().then((message) => {
              this.message = message;
              page.postBuild(this);
            });
          }
        }
      });
    }
  }

  update() {
    if (this.pageNumber < 1) {
      this.pageNumber = this.pages.length;
    }
    if (this.pageNumber > this.pages.length) {
      this.pageNumber = 1;
    }
    if (this.prevPageNumber !== this.pageNumber) {
      this.pages[this.pageNumber - 1]((page: EmbedPage) => {
        if (page) {
          this.message && this.message.edit(page.build()).catch();
          page.postBuild(this);
        }
        this.prevPageNumber = this.pageNumber;
      });
    }
  }

  delete() {
    this.message?.delete();
  }

  addInteraction(emoji: string, func: (parent: EmbedWrapper) => void) {
    this.currentInteractions.push(new EmbedReaction(this, emoji, func));
  }

  createEmbedPage(page: Discord.MessageEmbed, display = false) {
    return new EmbedPage(this, page, display);
  }
}

export class EmbedPage {
  public parent: EmbedWrapper;
  private page: Discord.MessageEmbed;
  private fields: EmbedField[] = [];
  private display: boolean;
  public currentInteractions: EmbedReaction[] = [];
  constructor(parent: EmbedWrapper, page: Discord.MessageEmbed, display = false) {
    this.parent = parent;
    this.page = page;
    this.display = display;
  }

  addField(field: EmbedField) {
    this.fields.push(field);
  }

  addTable(data: string[][]) {
    for (let index = 0; index < data.length; index++) {
      const column: string[] = data[index];
      const columnName = column.shift();
      if (columnName) {
        const tableField = new EmbedField(columnName, true);
        for (const row of column) {
          tableField.addRow(null, row);
        }
        this.addField(tableField);
      }
    }
  }

  public build(): Discord.MessageEmbed {
    this.fields.forEach(field => {
      const value = field.getValue();
      if (value !== '' || this.display) {
        this.page.addField(field.title, value, field.getInline());
      }
    });
    return this.page;
  }

  public postBuild(parent = this.parent) {
    this.parent = parent;
    if (this.parent) {
      if (this.parent.pages.length > 1) {
        const left = this.currentInteractions.find(a => a.emoji === '⬅️');
        if (!left) {
          this.addInteraction('⬅️', (parent: EmbedWrapper) => {
            parent.pageNumber-- && parent.update();
            return true;
          });
        }
        const right = this.currentInteractions.find(a => a.emoji === '➡️');
        if (!right) {
          this.addInteraction('➡️', (parent: EmbedWrapper) => {
            parent.pageNumber++ && parent.update();
            return true;
          });
        }
      }

      const currentEmojis = this.currentInteractions.map(inter => inter.emoji);

      const reactions = this.parent.message?.reactions.cache.filter(reaction => !(currentEmojis.includes(reaction.emoji.name)));
      if (reactions && reactions.size > 0) {
        for (const [,r] of reactions) {
          r.remove();
        }
      }

      this.parent.currentInteractions.forEach(reaction => {
        reaction.build(this);
      });
      this.currentInteractions.forEach(reaction => {
        reaction.build(this);
      });
    }
  }

  addInteraction(emoji: string, func: (parent: EmbedWrapper) => void) {
    this.currentInteractions.push(new EmbedReaction(this.parent, emoji, func));
  }

  createEmbedField(title: string, inline=false) {
    return new EmbedField(title, inline);
  }
}

class EmbedField {
  public title = '';
  public inline = false;
  private result = '';
  constructor(title: string, inline=false) {
    this.title = title;
    this.inline = inline;
  }

  addRow(name: any, value: any = '\u200b', newLines = 1) {
    this.result += name ? `**${name}**: ${value} ` : value;
    for (let index = 0; index < newLines; index++) {
      this.newline();
    }
  }

  newline() {
    this.result += '\n';
  }

  public getValue() {
    return this.result;
  }

  public getInline() {
    if (!this.inline) {
      return false;
    }
    return true;
  }
}

class EmbedReaction {
  private parent: EmbedWrapper;
  public emoji = '';
  public collector?: Discord.ReactionCollector;
  private func: (parent: EmbedWrapper) => void;
  constructor(parent: EmbedWrapper, emoji: string, func: (parent: EmbedWrapper) => void) {
    this.parent = parent;
    this.emoji = emoji;
    this.func = func;
  }

  public async build(page: EmbedPage) {
    this.parent = page.parent;
    if(!this.parent || !this.parent.message) {return;}
    this.parent.message.react(this.emoji);
    const filter = (reaction: { emoji: { name: string; }; }, user: Discord.User) => {
      if (this.parent && this.parent.message)
        {return reaction.emoji.name === this.emoji && user.id === this.parent.user;}
      return false;
    };
    this.collector = this.parent.message.createReactionCollector(filter, { time: 100000 });

    this.collector.on('collect', () => {
      if (page.parent && page.parent.message) {
        const userReactions = page.parent.message.reactions.cache.filter(reaction => reaction.users.cache.has(page.parent.user));
        try {
          for (const reaction of userReactions.values()) {
            reaction.users.remove(page.parent.user);
          }
          page.currentInteractions.forEach(interaction => {
            interaction.collector && interaction.collector.stop();
          });
        // eslint-disable-next-line no-empty
        } catch (error) {}
        this.func(this.parent);
      }
    });

    this.collector.on('end', async () => {
      if (this.collector) {
        if (this.parent.message) {
          try {
            this.collector.removeAllListeners();
          // eslint-disable-next-line no-empty
          } catch (error) {}
        }
      }
    });
  }
}
