import { ModuleEvent } from '../../../../moduleLoader/_types/moduleEvents';
import moduleLoader from '../../../../moduleLoader';

export default class ModuleReloader  {
  @ExtendModule('module')
  parent!: ModuleInterface;

  @OnModuleEvent(ModuleEvent.execute)
  @Permission(['Rat Catcher', 'bot'])
  @CommandInfo("Reloads the modules.")
  @HasAlias(['reload'])
  execute(command: DiscordInput, callback: (out: ModuleOutput) => void) {
    moduleLoader.modules.forEach((module: any) => {
      module.path && delete require.cache[require.resolve(module.path)];
      module.children.forEach((child: ModuleInterface) => {
        if (child.path) {
          delete require.cache[require.resolve(child.path)];
        }
      });
    });

    moduleLoader.resetReducer();
    command.message.channel.send("Modules have been Reloaded.");
    callback({ success: true });
  }
}
