import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class Ping {
  @ExtendModule('ping')
  parent!: ModuleInterface;

  @CommandInfo("Pings the bot to see if it is still responding.")
  @SlashCommand("ping", "Pings the bot to see if it is still responding.")
  @OnModuleEvent(ModuleEvent.execute)
  execute(command: DefaultInput, callback: (out: ModuleOutput) => void) {
    callback({ success: true, response: `Server Responded`, interaction: command?.interaction});
  }
}
