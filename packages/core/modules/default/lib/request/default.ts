import request from "request";
import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class RequestGetter {
  @ExtendModule('request')
  parent!: any;

  @OnModuleEvent(ModuleEvent.execute)
  execute(command: DefaultInput, callback: (out: ModuleOutput) => void){
    if (command.args && command.args.length > 0) {
      const url = command.args[0];
      const headers: request.Headers = command.args.length > 1 ? JSON.parse(command.args[1]) : {};
      const json = command.type?.toLowerCase() === 'html' ? false : command.args.length > 2 ? JSON.parse(command.args[2]) : true;
      const method =  command.type ? command.type?.toLowerCase() === 'html' ? 'GET' : command.type.toUpperCase() : 'GET';
      request(url, { method: method, json: json, headers }, (err: any, _: request.Response, body: string) => {
        if (err) { return callback({ success: false, result: err }); }
        callback({ success: true, result: body });
      });
    }
  }
}
