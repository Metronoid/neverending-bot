import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../../../../sequalize';

export interface ErrorCodeInterface extends Model {
  name: string;
  code: number;
  description: string;
  module: string;
}
export const ErrorCode = sequelize.define<ErrorCodeInterface>('ErrorCode', {
  code: {
    type: DataTypes.INTEGER,
    primaryKey: true
  },
  module: {
    type: DataTypes.STRING(64),
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING(64)
  },
  description: {
    type: DataTypes.STRING(256)
  }
}, {});
