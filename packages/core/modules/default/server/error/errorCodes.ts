import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";
import { ErrorCode, ErrorCodeInterface } from "./_schemes/error";

export default class ErrorHandler {
  @ExtendModule('error')
  parent!: ModuleInterface;

  @OnModuleEvent(ModuleEvent.insert)
  insertModel (model: ErrorCodeInterface, callback: (out: ModuleOutput) => void) {
    ErrorCode.findOrCreate({
      where: {
        module: model.module,
        code: model.code
      }
    }).then(([error]) => {
      error.name = model.name;
      error.description = model.description;
      error.save();
      callback && callback({success: true});
    });
  }

  @OnModuleEvent(ModuleEvent.execute)
  execute (command: DefaultInput, callback: (out: ModuleOutput) => void) {
    if (command.action && command.args) {
      const name = command.args[0];
      ErrorCode.findOne({
        where: {
          module: command.action,
          code: name,
        }
      }).then(errorCode => {
        if (!errorCode && command.action!== 'BASE') {
          ErrorCode.findOne({
            where: {
              module: 'BASE',
              code: name,
            }
          }).then(errorCode => {
            callback({ success: errorCode !== null, result: errorCode });
          });
        }
        else {
          callback({ success: errorCode !== null, result: errorCode });
        }
      });
    }
  }
}
