import { ServerSetting, ServerSettingInterface } from "./_schemes/serverSetting";
import { serverName, development, token } from "../../../../localSettings.json";
import { ModuleEvent } from "../../../../moduleLoader/_types/moduleEvents";

export default class ServerSettings {
  @ExtendModule('serverSettings')
  parent!: any;

  @UseModule('server')
  server!: any;

  @Store()
  settings?: ServerSettingInterface[];

  @OnModuleEvent(ModuleEvent.init)
  init(_: DefaultInput, callback: (out: ModuleOutput) => void) {
    const settings = [["token", token], ["development", String(development)]];
    const createNext = () => {
      const setting = settings.pop();
      const create = (name: string, value: string) => {
        ServerSetting.findOrCreate({
          where: {
            ServerName: serverName,
            name
          },
          defaults: {
            value,
            active: true
          }
        }).then(() => {
          callback({success: true});
        });
      };
      if(setting){
        create(setting[0], setting[1]);
      }else{
        callback({success: true});
      }
    };
    createNext();
  }

  @OnModuleEvent(ModuleEvent.execute)
  execute(command: DefaultInput, insertCallback: (out: ModuleOutput) => void){
    const returnCallback = (command: DefaultInput, insertCallback: (out: ModuleOutput) => void) => {
      // Try to find the setting in our store.
      let setting: ServerSettingInterface | undefined = undefined;
      if(this.settings)
        {setting = this.settings.find((setting: ServerSettingInterface) => command.args ? setting.name === command.args[0]: false);}
      insertCallback({ success: setting !== undefined, result: setting?.value });
    };

    if (!this.settings || this.settings.length === 0) {
      ServerSetting.findAll({
        where: {
          ServerName: serverName,
          active: true
        }
      }).then((result: any[]) => {
        if (result) {
          this.settings = result;
          returnCallback(command, insertCallback);
        }
      });
      return;
    }else{
      returnCallback(command, insertCallback);
    }
  }
}
