import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../../../../sequalize/index.js';
import { Server } from '../../_schemes/server.js';

export interface ServerSettingInterface extends Model {
  id: number;
  name: string;
  ServerName: string;
  active: boolean;
  value: string;
}

export const ServerSetting = sequelize.define<ServerSettingInterface>('ServerSetting', {
  id: {
    type: DataTypes.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: DataTypes.STRING(32),
    allowNull: false
  },
  active: {
    type: DataTypes.BOOLEAN,
    allowNull: false
  },
  value: {
    type: DataTypes.STRING(2024),
    allowNull: false
  }
});

ServerSetting.belongsTo(Server);
