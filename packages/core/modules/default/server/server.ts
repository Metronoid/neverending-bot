import { serverName } from "../../../localSettings.json";
import { ModuleEvent } from "../../../moduleLoader/_types/moduleEvents";
import { Server } from "./_schemes/server";

export default class ServerHandler {
  @ExtendModule('server')
  parent!: any;

  @OnModuleEvent(ModuleEvent.init)
  findOrCreateServer(_: any, callback: (out: ModuleOutput) => void){
    Server.findOrCreate({where: {
        name: serverName
    }}).then(() => {
        callback({success: true});
    });
  }
}
