import { sequelize } from '../../../../sequalize';
import { DataTypes, Model } from 'sequelize';
export interface ServerInterface extends Model {
  name: string;
}

export const Server = sequelize.define<ServerInterface>('Server', {
  name: {
    type: DataTypes.STRING(64),
    primaryKey: true
  }
}, {});
