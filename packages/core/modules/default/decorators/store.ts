interface storageKey {
  name: string,
  key: string
}

class GlobalStorage {
  private static _storage: GlobalStorage = new GlobalStorage();
  private _store: Map<string,Map<string, any>> = new Map<string,Map<string, any>>();

  public static get(s: storageKey) {
    return this._storage._store.get(s.name)?.get(s.key);
  }

  public static set(s: storageKey, value: any) {
    if (this._storage._store.has(s.name)) {
      if (this._storage._store.get(s.name)?.has(s.key)) {
        this._storage._store.get(s.name)?.delete(s.key);
      }
      this._storage._store.get(s.name)?.set(s.key, value);
    } else {
      const newMap = new Map<string, any>();
      this._storage._store.set(s.name, newMap.set(s.key, value));
    }
  }
}

export function Store(module?: string) {
  return (target: any, key: string) => {
    const name = (module ? module : target.parent.name);
    Object.defineProperty(target, key, {
      get: () => {
        return GlobalStorage.get({ name, key });
      },
      set: (v) => {
        GlobalStorage.set({ name, key }, v);
      }
    });
    return target;
  };
}

declare global {
  function Store(name?: string) : any;
}

const _global = (global) as any;
_global.Store = Store;