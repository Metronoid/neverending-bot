import "reflect-metadata";
import ModuleLoader from "../../../moduleLoader";

export const infoMetadataKey = Symbol("info");

export interface commandInfo{
  constructor: string;
  command: string;
  important?: boolean;
  usage?: string;
  aliases?: string[];
  description?: string;
}

export function SetCommandInfo(target: any, _: string, info: commandInfo) {
  const mod = ModuleLoader.modules.find(m => m.name?.toLowerCase() === target.parent.name?.toLowerCase());
  if (mod) {
    const commandInfos: commandInfo[] = Reflect.getOwnMetadata(infoMetadataKey, target.parent, target.parent.name) || [];

    const i = commandInfos.find(m => m.constructor === info.constructor && m.command === info.command);
    if (i) {
      i.constructor = info.constructor ?? i.constructor;
      i.command = info.command ?? i.command;
      i.important = info.important ?? i.important;
      i.aliases = info.aliases ?? i.aliases;
      i.usage = info.usage ?? i.usage;
      i.description = info.description ?? i.description;
    } else {
      commandInfos.push(info as commandInfo);
      Reflect.defineMetadata(infoMetadataKey, commandInfos, target.parent, target.parent.name);
    }
  }
}

function CommandInfo(description: string, usage: string, important = false) {
  return (target: any, key: string) => {
    return SetCommandInfo(target, key, {constructor: target.constructor.name, command: key, description, usage, important} as commandInfo);
  };
}

declare global {
  function CommandInfo(description: string, usage?: string, important?: boolean): any;
}

const _global = (global) as any;
_global.CommandInfo = CommandInfo;
