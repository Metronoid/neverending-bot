import moduleLoader from "../../../moduleLoader";
import { extendedObject, dependenciesMetadataKey } from "./extends";

//TODO: Warning for Circular Dependency

export function UseModule(module?: string) {
  return (target: extendedObject, key: string) => {
    if(target.parent) {
      const k = (module ? module : key).toLowerCase();
      if (k) {
        const mod = moduleLoader.modules.find(m => m.name?.toLowerCase() === k);
        Object.defineProperty(target, key, {
          configurable: false,
          get: () => mod
        });

        if (mod) {
          const existingdependencies: ModuleInterface[] = Reflect.getOwnMetadata(dependenciesMetadataKey, target.parent, target.parent.name) || [];
          if (existingdependencies.findIndex(m => m.name === mod.name) === -1) {
            existingdependencies.push(mod);
            Reflect.defineMetadata(dependenciesMetadataKey, existingdependencies, target.parent, target.parent.name);
          }
        }
      }
    }
    return target;
  };
}

declare global {
  function UseModule(module?: string) : any;
}

const _global = (global) as any;
_global.UseModule = UseModule;
