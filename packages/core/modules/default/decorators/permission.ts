import "reflect-metadata";
function Permission(roles: string[]) {
  return (target: any, key: string) => {
    const copy = target[key].bind(target);

    target[key] = (command: DiscordInput, callback: (ModuleOutput: ModuleOutput) => void) => {
      if (command.message.guild) {
        command.message.guild.members.fetch(command.message.author).then((discordUser: any) => {
          if (discordUser.roles.cache.some((role: any) => role && roles && roles.map(r => r?.toLowerCase()).includes(role.name?.toLowerCase()))) {
            copy(command, callback);
          }
          else {
            callback({ success: false, response: "**You do not have Permission to use this command!**" });
          }
        });
      } else {
        callback({ success: false, response: "**Commands that require permissions can not be used in DMs!**" });
      }
    };
  };
}

declare global {
  function Permission(roles: string[]): any;
}

const _global = (global) as any;
_global.Permission = Permission;