import "reflect-metadata";
import { commandInfo, SetCommandInfo } from "./info";

export enum specialAliases {
  default = "default",
  arguments = "args"
}

function HasAlias(alias: string[]) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return (target: any, key: string, descriptor: PropertyDescriptor) => {
    SetCommandInfo(target, key, { constructor: target.constructor.name, command: key, aliases: alias } as commandInfo);
    // the new constructor behaviour
    const copy = target[key].bind(target);


    const func = (command: DefaultInput, callback: any) => {
      if (command.args && command.args.length > 0) {
        const name: string = command.args[0];
        if (name && alias.some(w => w.toLowerCase().includes(name) || command.action === w || w === specialAliases.arguments)) {
          copy(command, callback);
        } else {
          callback({success: false});
        }
      } else {
        if (alias.some(w => command.action === w || specialAliases.default === w)) {
          copy(command, callback);
        } else {
          callback({success: false});
        }
      }
    };

    target[key] = func;
    descriptor.value = func;
    //return target;
  };
}
declare global {
  function HasAlias(alias: string[]): any;
}

const _global = (global) as any;
_global.HasAlias = HasAlias;
