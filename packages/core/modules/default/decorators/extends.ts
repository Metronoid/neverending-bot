import "reflect-metadata";
import moduleLoader from "../../../moduleLoader";

export const dependenciesMetadataKey = Symbol("dependencies");

export interface extendedObject extends Record<string, unknown> {
  parent: ModuleInterface;
}

function ExtendModule(module?: string) {
  return (target: Record<string, unknown>, key: string) => {
    const k = (module ? module : key);
    const mod = moduleLoader.modules.find(m => m.name.toLowerCase() === k.toLowerCase());

    if(!(key in target)){
      Object.defineProperty(target, key, {
        configurable: false,
        value: mod,
      });
    }
    return target;
  };
}

declare global {
  function ExtendModule(module?: string): any;
}

const _global = (global) as any;
_global.ExtendModule = ExtendModule;
