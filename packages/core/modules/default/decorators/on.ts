/* eslint-disable @typescript-eslint/ban-types */
import "reflect-metadata";
import ModuleLoader from "../../../moduleLoader";
import { ModuleEvent } from "../../../moduleLoader/_types/moduleEvents";
import { extendedObject } from "./extends";

function OnModuleEvent(event: ModuleEvent) {
  return (target: extendedObject, key: string, descriptor: TypedPropertyDescriptor<Function>) => {
    const module = ModuleLoader.modules.find(m => m.name?.toLowerCase() === target.parent.name?.toLowerCase());
    if(module){
      Validate(module, descriptor);
      module.subscribe(event, target, key);
    }
  };
}

function Validate(module: ModuleInterface, descriptor: TypedPropertyDescriptor<Function>){
  if(descriptor.value instanceof Function){
    if(descriptor.value.length < 2){
      console.error(`${module.name}.${descriptor.value.name} is invalid. Too few arguments`);
      process.exit();
    }
  }else{
    console.error(`Can not find function for ${module.name}`);
    process.exit();
  }
}

declare global {
  function OnModuleEvent(event: ModuleEvent): any;
}

const _global = (global) as any;
_global.OnModuleEvent = OnModuleEvent;
