//sequalize handles our database connection https://sequelize.org/
import * as seq from 'sequelize';
//localSettings containing development boolean that tells us if we clear the database on first run or not.
import { development, PG } from "../localSettings.json";

export const sequelize = new seq.Sequelize(PG.DATABASE, PG.USER, PG.PASSWORD, {
  host: PG.HOST,
  port: Number(PG.PORT),
  dialect: PG.DIALECT as seq.Dialect,
  logging: console.log
});

/**
 * This function is used to initialize the database connection
 */
export function startSequalize(callback: { (): void; }) {
  sequelize.sync(development ? { force: true } : { alter: true }).then(() => {
    console.log('Started Sequalize connection with Database');
    callback();
  });
}
